json.extract! login, :id, :user_id, :ip_address, :user_agent, :device_id, :created_at, :updated_at
json.url login_url(login, format: :json)

json.extract! address_verification, :id, :user_id, :country_id, :city, :state, :street, :building, :number, :status, :note, :created_at, :updated_at
json.url address_verification_url(address_verification, format: :json)

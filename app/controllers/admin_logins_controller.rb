class AdminLoginsController < ApplicationController
  before_action :require_login
  before_action :set_admin_login, only: [:show, :edit, :update, :destroy]

  # GET /admin_logins
  # GET /admin_logins.json
  def index
    if current_user.roleid == 1
       @admin_logins = AdminLogin.all
    else
        redirect_to root_path , notice: "not allowed" 
    end 
  end

  # show admin's active sessions
  # works only if current user is an admin
  # @param [Integer] admin_id
  def admin_active_sessions
    if current_user.roleid == 1 or current_user.id == params[:id].to_i
      @logins = AdminLogin.where("admin_id =? ",params[:id])
    else
      redirect_to root_path , notice: "not allowed" 
    end
  end

  # GET /admin_logins/1
  # GET /admin_logins/1.json
  def show
    if current_user.roleid != 1 and current_user.id != params[:id].to_i 
      redirect_to root_path , notice: "not allowed" 
    end
  end

  # GET /admin_logins/new
  def new
    @admin_login = AdminLogin.new
  end

  # GET /admin_logins/1/edit
  def edit
  end

  # POST /admin_logins
  # POST /admin_logins.json
  def create
    @admin_login = AdminLogin.new(admin_login_params)

    respond_to do |format|
      if @admin_login.save
        format.html { redirect_to @admin_login, notice: 'Admin login was successfully created.' }
        format.json { render :show, status: :created, location: @admin_login }
      else
        format.html { render :new }
        format.json { render json: @admin_login.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin_logins/1
  # PATCH/PUT /admin_logins/1.json
  def update
    respond_to do |format|
      if @admin_login.update(admin_login_params)
        format.html { redirect_to @admin_login, notice: 'Admin login was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_login }
      else
        format.html { render :edit }
        format.json { render json: @admin_login.errors, status: :unprocessable_entity }
      end
    end
  end

  def lock_page
    render :layout => false
  end

  def check_to_unlock
    @user = Admin.authenticate(current_user.email,params[:unlock][:password])
    if !(@user)  
      @user_sessions = Login.where(user_id: current_user.id).all
        if (@user_sessions  != nil)
          @user_sessions.all.each do |user_sessions|
              user_sessions.destroy
          end
        end
      cookies.delete(:auth_token)
      cookies.delete(:device_id)
      session[:refered_by] = nil
      #session[:last_visit] = nil
      sign_out
      redirect_to sign_in_path 
         #redirect_to lock_page_path , notice: "wrong password"   
    else
      session[:last_visit] = Time.now
      redirect_to root_path , notice: "welcome again!"   
    end
  end

  # DELETE /admin_logins/1
  # DELETE /admin_logins/1.json
  def destroy
    @admin_login.destroy
    respond_to do |format|
      format.html { redirect_to admin_logins_url, notice: 'Admin login was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_login
      @admin_login = AdminLogin.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_login_params
      params.require(:admin_login).permit(:admin_id, :ip_address, :user_agent, :device_id, :operation_type)
    end
end

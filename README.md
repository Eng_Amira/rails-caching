# Prerequisites
* Ruby version:    2.5.1
* Rails Version:   5.2.1
* Used Gems:

  gem 'clearance' Used for Authentication

  gem 'mini_magick'  Used for ActiveStorage variant

  gem 'device_detector' Used to detect devise details

  gem 'maxminddb' Used to read IP details

  gem 'active_model_otp' Used to implement Google's MFA authenticator

  gem 'rqrcode' Used to implement QR Code

  gem 'jwt' Used for Authentice user via token

  gem 'rspec-rails' ~> 3.5  Test

  gem 'shoulda-matchers' ~> 3.1 Test 

  gem 'factory_bot_rails' ~> 4.0 Test

  gem 'faker' Test
  
  gem 'database_cleaner' Test

# Main Programming Parts
* Sign Up

 Admin can add new admins. 

 Code documentation Link: http://0.0.0.0:8808/docs/AdminsController#add_admin-instance_method

 Code documentation Link: http://0.0.0.0:8808/docs/AdminsController#add_admin_post-instance_method

* Sign In

 Admins can sign in using Valid email and password.

 Code documentation Link: http://0.0.0.0:8808/docs/Clearance/SessionsController#create-instance_method

* confirm sign in via two factor authentication

 Code documentation Link: http://0.0.0.0:8808/docs/AdminsController#get_two_factor-instance_method

 Code documentation Link: http://0.0.0.0:8808/docs/AdminsController#send_confirmation_email-instance_method

 Code documentation Link: http://0.0.0.0:8808/docs/AdminsController#post_two_factor-instance_method


* Send email to Reset Password.

Code documentation Link: http://0.0.0.0:8808/docs/Clearance/PasswordsController#create-instance_method

* Reset Admin's Password.

Code documentation Link: http://0.0.0.0:8808/docs/Clearance/PasswordsController#update-instance_method


* UnLock Admin account

 UnLock account after Number of failed attempts.

 API documentation link: https://payers-userpanel.herokuapp.com/api/index.html#api-Authentication-unlockaccount

 Code documentation Link: http://0.0.0.0:8808/docs/AdminsController#unlockaccount-instance_method

* Show Admin profile 

 Code documentation Link: http://0.0.0.0:8808/docs/AdminsController#show-instance_method

* Edit admin profile

 Code documentation Link: http://0.0.0.0:8808/docs/AdminsController#edit-instance_method

 Code documentation Link: http://0.0.0.0:8808/docs/AdminsController#update-instance_method

* Choose another two factor authentication 

Code documentation Link: http://0.0.0.0:8808/docs/AdminsController#change_confirmation_code-instance_method

Code documentation Link: http://0.0.0.0:8808/docs/AdminsController#confirm_google_code-instance_method

* Show Admin Log

 Code documentation Link: http://0.0.0.0:8808/docs/AdminWatchdogsController#admin_log-instance_method

* Show Admin's Active Sessions

API documentation link:   https://payers-userpanel.herokuapp.com/api/index.html#api-Log-Getactivesessions

 Code documentation Link: http://0.0.0.0:8808/docs/AdminLoginsController#admin_active_sessions-instance_method

* Show National ID Verification Documents

Code documentation Link: http://0.0.0.0:8808/docs/NationalidVerificationsController

* Show Address Verification Documents

Code documentation Link: http://0.0.0.0:8808/docs/AddressVerificationsController

* Show Selfie Verification

Code documentation Link: http://0.0.0.0:8808/docs/SelfieVerificationsController

* Create New Country

API documentation link: https://payers-userpanel.herokuapp.com/api/index.html#api-Countries-postCountry

Code documentation Link: http://0.0.0.0:8808/docs/CountriesController#new-instance_method

Code documentation Link: http://0.0.0.0:8808/docs/CountriesController#create-instance_method

* Delete Country

API documentation link: https://payers-userpanel.herokuapp.com/api/index.html#api-Countries-DeleteCountry

Code documentation Link: http://0.0.0.0:8808/docs/CountriesController#destroy-instance_method

* Show Country Details

API documentation link: https://payers-userpanel.herokuapp.com/api/index.html#api-Countries-GetCountry

Code documentation Link: http://0.0.0.0:8808/docs/CountriesController#show-instance_method

* List All Countries

API documentation link: https://payers-userpanel.herokuapp.com/api/index.html#api-Countries-GetCountries

Code documentation Link: http://0.0.0.0:8808/docs/CountriesController#index-instance_method

* Edit Country Data

API documentation link: https://payers-userpanel.herokuapp.com/api/index.html#api-Countries-PutCountry

Code documentation Link: http://0.0.0.0:8808/docs/CountriesController#edit-instance_method

Code documentation Link: http://0.0.0.0:8808/docs/CountriesController#update-instance_method

* List All Users

API documentation link: https://payers-userpanel.herokuapp.com/api/index.html#api-User-GetUsers

Code documentation Link: http://0.0.0.0:8808/docs/UsersController#index-instance_method

* Show user profile 

 API documentation link: https://payers-userpanel.herokuapp.com/api/index.html#api-User-GetUser

 Code documentation Link: http://0.0.0.0:8808/docs/UsersController#show-instance_method

* Edit user profile

 API documentation link: https://payers-userpanel.herokuapp.com/api/index.html#api-User-PutUser

 Code documentation Link: http://0.0.0.0:8808/docs/UsersController#edit-instance_method

 Code documentation Link: http://0.0.0.0:8808/docs/UsersController#update-instance_method

* Sign Out

 Code documentation Link: http://0.0.0.0:8808/docs/Clearance/SessionsController#destroy-instance_method

# Test Code

* Create User
* Authenticate User
* list all users
* Create New Country
* List all Countries
* Show country Details
* Edit Country Details
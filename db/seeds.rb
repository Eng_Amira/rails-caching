# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Country.create([{
    short_code: "EGY",
    Full_Name: "Egypt",
    Phone_code: "002",
    Currency:  "EGP",
    language: "Arabic",
    active: "1",
            },
{
    short_code: "SAU",
    Full_Name: "Saudi Arabia	",
    Phone_code: "00966",
    Currency:  "SAR",
    language: "Arabic",
    active: "1",
            }
        ]) 
        
Admin.create([{
            email: "amiraaa.elfayome@servicehigh.com",
            password: "Amira123456",
            username: "Amiraaaa",
            status: 1,
            account_number: "PS100000005",
            remember_token: Clearance::Token.new,
            country_id: 9
        }
        ])
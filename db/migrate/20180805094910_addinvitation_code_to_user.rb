class AddinvitationCodeToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :invitation_code , :string
    User.all.each { |user| user.update_attribute(:invitation_code, SecureRandom.hex(4)) }  
  end
end

class AddOperationTypeToWatchdog < ActiveRecord::Migration[5.2]
  def change
    add_column :watchdogs, :operation_type, :string
  end
end

require 'rails_helper'

RSpec.describe "admin_watchdogs/new", type: :view do
  before(:each) do
    assign(:admin_watchdog, AdminWatchdog.new(
      :admin_id => 1,
      :ipaddress => "MyText",
      :operation_type => "MyString"
    ))
  end

  it "renders new admin_watchdog form" do
    render

    assert_select "form[action=?][method=?]", admin_watchdogs_path, "post" do

      assert_select "input[name=?]", "admin_watchdog[admin_id]"

      assert_select "textarea[name=?]", "admin_watchdog[ipaddress]"

      assert_select "input[name=?]", "admin_watchdog[operation_type]"
    end
  end
end
